package com.sofkau.ToDo.controller;

import com.sofkau.ToDo.service.ToDoService;
import com.sofkau.ToDo.model.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ToDoController {

    @Autowired
    private ToDoService todoService;

    @GetMapping(value = "api/todos")
    public Iterable<ToDo> list(){//paso los metodos del service
        return todoService.list();
    }

    @GetMapping (value = "api/{id}/todos")
    public ToDo get(@PathVariable("id") Long id){
        return todoService.get(id);//paso los metodos del service
    }

    @PostMapping(value = "api/todo")
    public ToDo save(@RequestBody ToDo todo){//paso los metodos del service
        return todoService.save(todo);
    }

    @PutMapping(value = "api/todo")
    public ToDo update(@RequestBody ToDo todo){//paso los metodos del service
        if(todo.getId()!= null){
            return todoService.save(todo);
        } throw new RuntimeException("No existe el Id paraa actualizar");

    }

    @DeleteMapping (value = "api/{id}/todos")
    public void delete(@PathVariable("id")Long id){//paso los metodos del service
        todoService.delete(id);
    }
}
