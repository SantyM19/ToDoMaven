package com.sofkau.ToDo.service;

import com.sofkau.ToDo.model.ToDo;
import com.sofkau.ToDo.repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ToDoService {

    //inyectamos el repositorio
    @Autowired
    private ToDoRepository repository;
    //.....................

    public Iterable<ToDo> list(){//listar todos
        return repository.findAll();
    }

    public ToDo save(ToDo todo){//guardar todos
        return repository.save(todo);
    }

    public void delete(Long id){//eliminar
        repository.delete(get(id));
    }

    public ToDo get(Long id){//optener id para eliminar
        return repository.findById(id).orElseThrow();
    }
}
