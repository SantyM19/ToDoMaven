package com.sofkau.ToDo.repository;

import com.sofkau.ToDo.model.ToDo;
import org.springframework.data.repository.CrudRepository;

public interface ToDoRepository extends CrudRepository<ToDo,Long> {

}